#!/usr/bin/env python3
from termcolor import colored
from termcolor import cprint
from pyfiglet import figlet_format
from tqdm import tqdm
from constants import (IF_CV, ESI_CV,
                       IF_TA, RBB_HOR, ESI_TA,
                       RBB_SLO, TEMP_EVPN_ELAN, PORT,
                       USER, PSW, TEMP_VPLS_ELAN)
from jnpr.junos.exception import ConfigLoadError
from jnpr.junos.exception import CommitError
from jnpr.junos.exception import ConnectAuthError
from jnpr.junos.exception import ConnectRefusedError
from jnpr.junos.exception import ConnectTimeoutError
from jnpr.junos.exception import ConnectError
from jnpr.junos.utils.config import Config
from jnpr.junos import Device
from getpass import getpass
from jinja2 import Environment, FileSystemLoader


def create_esi(service_id, interface):

    if interface == IF_CV:
        esi_site = ESI_CV
    else:
        esi_site = ESI_TA

    if len(service_id) < 6:
        service_id = "0" + service_id
    esi = esi_site

    for a in range(0, len(service_id)-1, 2):
        esi = esi + ":" + service_id[a]+service_id[a+1]
    return esi


def create_vpls_configuration(rac):
    load_template = FileSystemLoader('Templates')
    env = Environment(loader=load_template)
    template_elan = env.get_template(TEMP_VPLS_ELAN)

    vpls_name = rac['instance']
    vpls_name = vpls_name.replace('EVPN-', 'VPLS-')

    cnfg_to_apply = template_elan.render(
        routing_instance=[
            {
                'vpls_name': vpls_name,
                'rt': rac['rt'],
                'rd': rac['rd'],
                'evpn': rac['instance']
            }])

    for interface in rac['found_interfaces']:
        if rac['ip_mgmt'] in (RBB_HOR, RBB_SLO):
            if interface['name'] == IF_CV:
                cnfg_to_apply += template_elan.render(
                    mh_site_cv=[
                        {
                            'vpls_name': vpls_name,
                            'interface': interface['name']
                            + '.' + interface['unit']
                        }])
            elif interface['name'] == IF_TA:
                cnfg_to_apply += template_elan.render(
                    mh_site_teco=[
                        {
                            'vpls_name': vpls_name,
                            'interface': interface['name']
                            + '.' + interface['unit']
                        }])
            else:
                print(
                    colored("\nError: The interface {} in the {}"
                            " is associated to the service but "
                            "does not belong to any of the "
                            "multihoming interfaces".format(
                                interface['name'] + '.'
                                + interface['unit'],
                                rac['hostname']), "red"
                            )
                        )
        else:
            cnfg_to_apply += template_elan.render(
                sh_site=[
                    {
                        'vpls_name': vpls_name,
                        'interface': interface['name']
                        + '.' + interface['unit']
                    }])

    for interface in rac['found_interfaces']:
        if interface['plaza'] == 'large':
            cnfg_to_apply += template_elan.render(
                elan_large=[
                    {
                        'interface': interface['name'],
                        'unit': interface['unit'],
                        'svlan': interface['svlan'],
                        'cvlan': interface['cvlan']
                    }])

        elif interface['plaza'] == 'small':
            cnfg_to_apply += template_elan.render(
                elan_small=[
                    {
                        'interface': interface['name'],
                        'unit': interface['unit'],
                        'vlan': interface['vlan']
                    }])

        if interface['description'] is not None:
            description = (
                interface['description'].replace('EVPN', 'VPLS')
            )
            cnfg_to_apply += template_elan.render(
                interface_description=[
                    {
                        'interface': interface['name'],
                        'unit': interface['unit'],
                        'description': description
                    }])

        if interface['input_filter'] is not None:
            input_filter = (
                interface['input_filter']).replace('bridge', 'vpls')
            cnfg_to_apply += template_elan.render(
                interface_input_filter_vpls=[
                    {
                        'interface': interface['name'],
                        'unit': interface['unit'],
                        'input_filter': input_filter
                    }
                ]
            )

    return cnfg_to_apply


def create_evpn_configuration(rac, service_id):
    load_template = FileSystemLoader('Templates')
    env = Environment(loader=load_template)
    template_elan = env.get_template(TEMP_EVPN_ELAN)

    evi_name = rac['instance']
    evi_name = evi_name.replace('VPLS-', 'EVPN-')
    cnfg_to_apply = template_elan.render(
        routing_instance=[
            {
                'evi': evi_name,
                'rt': rac['rt'],
                'rd': rac['rd'],
                'vpls': rac['instance']
            }])

    for interface in rac['found_interfaces']:
        cnfg_to_apply += template_elan.render(
            routing_instance_interfaces=[
                {
                    'evi': evi_name,
                    'interface': interface['name']
                    + '.' + interface['unit']
                }])

    for interface in rac['found_interfaces']:
        if interface['plaza'] == 'large':
            cnfg_to_apply += template_elan.render(
                elan_large=[
                    {
                        'interface': interface['name'],
                        'unit': interface['unit'],
                        'svlan': interface['svlan'],
                        'cvlan': interface['cvlan']
                    }])

        elif interface['plaza'] == 'small':
            if rac['ip_mgmt'] == RBB_HOR:
                if interface['name'] in (IF_CV, IF_TA):
                    esi = create_esi(service_id,
                                     interface['name'])
                    cnfg_to_apply += template_elan.render(
                        elan_mh_hor=[
                            {
                                'interface': interface['name'],
                                'unit': interface['unit'],
                                'vlan': interface['vlan'],
                                'esi': esi
                            }])
                else:
                    print(
                        colored("\nError: The interface {} in the {}"
                                " is associated to the service but "
                                "does not belong to any of the "
                                "multihoming interfaces".format(
                                    interface['name'] + '.'
                                    + interface['unit'],
                                    rac['hostname']), "red"
                                )
                            )

            elif rac['ip_mgmt'] == RBB_SLO:
                if interface['name'] in (IF_CV, IF_TA):
                    esi = create_esi(service_id, interface['name'])
                    cnfg_to_apply += template_elan.render(
                        elan_mh_slo=[
                            {
                                'interface': interface['name'],
                                'unit': interface['unit'],
                                'vlan': interface['vlan'],
                                'esi': esi
                            }])
                else:
                    print(
                        colored("\nError: The interface {} in the {}"
                                " is associated to the service but "
                                "does not belong to any of the "
                                "multihoming interfaces".format(
                                    interface['name'] + '.'
                                    + interface['unit'],
                                    rac['hostname']), "red"
                                )
                            )
            else:
                cnfg_to_apply += template_elan.render(
                    elan_small=[
                        {
                            'interface': interface['name'],
                            'unit': interface['unit'],
                            'vlan': interface['vlan']
                        }])

        if interface['description'] is not None:
            description = (
                interface['description'].replace('VPLS', 'EVPN')
            )
            cnfg_to_apply += template_elan.render(
                interface_description=[
                    {
                        'interface': interface['name'],
                        'unit': interface['unit'],
                        'description': description
                    }])

        if interface['input_filter'] is not None:
            input_filter = (
                interface['input_filter']).replace('vpls', 'bridge')
            cnfg_to_apply += template_elan.render(
                interface_input_filter_evpn=[
                    {
                        'interface': interface['name'],
                        'unit': interface['unit'],
                        'input_filter': input_filter
                    }
                ]
            )

    return cnfg_to_apply


def deploy_configuraton(rac, cnfg_to_apply, commit_comment):
    try:
        with Device(host=rac['ip_mgmt'], user=USER, passwd=PSW,
                    port=PORT) as dev:
            with Config(dev, mode="exclusive") as conf:
                conf.load(cnfg_to_apply, merge=True, format="set")
                if conf.diff() is None:
                    print(colored("\u00BB", "yellow") +
                          colored(" Configuration already up to"
                                  " date.", "green"))
                else:
                    print(colored("\u00BB", "yellow") +
                          colored(" Show | compare: ", "green"))
                    conf.pdiff()
                    print(colored("\u00BB", "yellow") +
                          colored(" Commit Check...", "green"))
                    if conf.commit_check(timeout=360):
                        print(colored("\u00BB", "yellow") +
                              colored(" Commit Confirm...", "green"))
                        conf.commit(
                            confirm=5,
                            timeout=360,
                            comment=commit_comment)
                        print(colored("\u00BB", "yellow") +
                              colored(" Saving Configuration...", "green"))
                        conf.commit(
                            comment=commit_comment,
                            timeout=360)
                        print(colored("\u00BB", "yellow") +
                              colored(" The configuration "
                                      "was applied successfully.", "green"))
                    else:
                        print(
                            colored("\u00BB", "yellow") +
                            colored(
                                " Error: An error occurred during the "
                                "'commit check'...", "red"))
                        print(
                            colored("\u00BB", "yellow") +
                            colored(" Executing rollback...", "red"))
                        conf.rollback()
    except (ConfigLoadError, CommitError) as err:
        print(err)
    except ConnectAuthError:
        tqdm.write(
            colored("\u00BB", 'yellow') +
            " Nodo: {} ".format(rac['ip_mgmt']) +
            colored("\u2718", 'red') +
            colored(" - Authentication error!.", 'red'))
    except ConnectRefusedError:
        tqdm.write(
            colored("\u00BB", 'yellow') +
            " Nodo: {} ".format(rac['ip_mgmt']) +
            colored("\u2718", 'red') +
            colored(" - Connection Refused!.", 'red'))
    except ConnectTimeoutError:
        tqdm.write(
            colored("\u00BB", 'yellow') +
            " Nodo: {} ".format(rac['ip_mgmt']) +
            colored("\u2718", 'red') +
            colored(" - NETCONF connection timed out!.",
                    "red"))
    except ConnectError as error:
        tqdm.write(
            colored("\u00BB", 'yellow') +
            " Nodo: {} ".format(rac['ip_mgmt']) +
            colored("\u2718", 'red') +
            colored(" - Other connection error: %s."
                    % str(error), 'red'))
    except Exception as error:
        tqdm.write(
            colored("\u00BB", 'yellow') +
            " Nodo: {} ".format(rac['ip_mgmt']) +
            colored("\u2718", 'red') +
            colored(" - Other error: %s."
                    % str(error), 'red'))


def banner_vpls_to_evpn():
    banner = "    VPLS to EVPN\n            MIGRATOR"
    cprint(figlet_format(banner, font="standard"), "blue")
    print(colored("Welcome", "green"))


def banner_evpn_to_vpls():
    banner = "            ROLLBACK\n    EVPN to VPLS\n            MIGRATOR"
    cprint(figlet_format(banner, font="standard"), "blue")
    print(colored("Welcome", "green"))


def get_user_password_target():

    user = input(colored("\u00BB", "yellow")+' Username: ')
    passwd = getpass(colored("\u00BB", "yellow")+' Password: ')
    target = input(colored("\u00BB", "yellow")+' ID de servicio: ')
    return user, passwd, target


def get_service_id():
    target = input(
        colored("\u00BB", 'yellow') +
        ' Ingrese el ID de Servicio: '
        )
    return target
