# Script para la migracion de servicios VPLS a EVPN en red Fibercorp

# IMPORTANTE !!!!
# Es necesario crear las carpetas "ConfigBackup", "Reports" y "Data". Adicionalmente sobre la carpeta "Data" debemos
# crear el archivo "data.json":
# evpn_migrator
#   |
#   |--> ConfigBackup
#   |--> Reports
#   |--> Data
#         |--> data.json