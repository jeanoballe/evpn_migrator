#!/usr/bin/env python3
from termcolor import colored
from constants import DATA_JSON
import json
from utils import create_vpls_configuration
from utils import deploy_configuraton
from utils import banner_evpn_to_vpls
from utils import get_service_id


def migrator_evpn_to_vpls(service):
    for rac in service:
        if rac['service_type'] == 'l2-evpn':
            print(colored("["+"#" * 88 + "]", 'blue'))
            print(
                colored("\n\u00BB", "yellow") +
                colored(" Preparing to migrate the service on the node:"
                        " {}...".format(rac['hostname']), "green"))
            commit_comment = "Migration_EVPN_to_VPLS"
            cnfg_to_apply = create_vpls_configuration(rac)
            print(
                colored("\u00BB", "yellow") +
                colored(" The following configuration will be applied: \n",
                        'green'))
            print(cnfg_to_apply+'\n')
            ready = False
            while ready is False:
                menu_input = input(
                    colored("\u00BB", "yellow") +
                    colored(" ¿ Do you want to apply the configuration ?"
                            " (y/n): ", "green"))
                if menu_input.lower() == "y":
                    ready = True
                    deploy_configuraton(rac, cnfg_to_apply, commit_comment)
                elif menu_input.lower() == "n":
                    print("Configuration discarded.\n")
                    ready = True
                else:
                    print("Please enter a valid option.")
        else:
            print(
                colored("\n["+"#" * 88 + "]", 'blue'))
            print("Error - Service is NOT a EVPN")


def main():

    services_found = []
    banner_evpn_to_vpls()
    print(colored("\u00BB", "yellow")+" Loading file " + DATA_JSON + "...")
    with open(DATA_JSON) as f:
        try:
            services_found = json.load(f)
        except Exception as e:
            print("Error: %s" % str(e))

    if len(services_found):
        service_id = None
        while service_id is None:
            service_id = get_service_id()
            if service_id.isdigit() is False:
                print("El ID de servicio debe ser un numero."
                      " Ingreselo nuevamente.")
                service_id = None

        if services_found[0]['rt'].replace('target:64600:', '') == service_id:
            print(colored("\n["+"#" * 88 + "]", 'blue'))
            print(
                colored("["+"#" * 32, 'blue') +
                colored(" \u25BC ", "yellow") +
                colored("SERVICE MIGRATION", 'green') +
                colored(" \u25BC ", "yellow") +
                colored("#" * 33 + "]", 'blue'))
            migrator_evpn_to_vpls(services_found)
        else:
            print(
                "Error: The service ID does not match with the service id in "
                "the Data/data.json file.")
    else:
        print(
            "Error: It is not possible to create the report or migrate the "
            "service because the " + DATA_JSON + " file is empty")


if __name__ == "__main__":
    main()
