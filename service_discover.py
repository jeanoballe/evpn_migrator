#!/usr/bin/env python3
# The netauto dir contains netnodes.py
import sys
import yaml
from termcolor import colored
from termcolor import cprint
from pyfiglet import figlet_format
from tqdm import tqdm
from getpass import getpass
import json
from constants import (USER, PSW, DIR_CONF_BACKUP,
                       DIR_REPORTS, DATA_JSON, NODES,
                       DIR_NETAUTO)
sys.path.append(DIR_NETAUTO)
from net_nodes import create_network_node
from net_nodes import create_report


def find_service(nodes, service_id, user, psw):
    services_found = []
    for netnode in tqdm(nodes['RACs'], ncols=90):
        try:
            rac = create_network_node(
                ip_mgmt=netnode['ip_mgmt'],
                vendor=netnode['vendor'],
                model=netnode['model'],
                user=user,
                psw=psw,
                port="443",
                asw=None
                )

            service = rac.service_status(
                service_id=service_id, get_config=True,
                dir_path=(DIR_CONF_BACKUP)
                )

            if service is not None:
                services_found.append(service)

        except ValueError as error:
            tqdm.write(
                colored(
                  "\u00BB", 'yellow')+" Nodo: {} ".format(netnode['ip_mgmt']) +
                colored("\u2718", 'red') +
                colored(" - Other Error: %s." % str(error), 'red'))
        continue

    return services_found


def banner():
    banner = "         SERVICE\n    DISCOVERY"
    cprint(figlet_format(banner, font="standard"), "blue")
    print(colored("Welcome", "green"))


def get_user_password_target():
    user = input(colored("\u00BB", 'yellow')+' Username: ')
    passwd = getpass(colored("\u00BB", 'yellow')+' Password: ')
    target = input(colored("\u00BB", 'yellow')+' ID de servicio: ')
    return user, passwd, target


def get_service_id():
    target = input(
        colored("\u00BB", 'yellow') +
        ' Ingrese el ID de Servicio: '
        )
    return target


def main():
    services_found = []

    with open(NODES) as f:
        try:
            nodes = yaml.safe_load(f)
        except yaml.YAMLError as exc:
            print(exc)

    banner()

    service_id = None
    while service_id is None:
        service_id = get_service_id()
        if service_id.isdigit() is False:
            print("El ID de servicio debe ser un numero."
                  " Ingreselo nuevamente.")
            service_id = None

    # Busqueda de servicio PRE-TAREA y POST-TAREA
    print(
        colored("\n["+"#" * 88+"]", 'blue'))
    print(
        colored("["+"#" * 16, 'blue') +
        colored(" \u25B6 ", 'yellow') +
        colored("Relevando existencia del servicio en red Fibercorp",
                'green') +
        colored(" \u25C0 ", 'yellow') +
        colored("#" * 16 + "]", 'blue'))
    print(
        colored("["+"#" * 88+"]", 'blue'))

    services_found = find_service(
                        nodes=nodes,
                        service_id=service_id,
                        user=USER, psw=PSW
                        )

    if len(services_found):
        print(
            colored("\n["+"#" * 88 + "]", 'blue'))
        print(
            colored("["+"#" * 34, 'blue') +
            colored(" \u25BC ", 'yellow') +
            colored("SUMMARY REPORT", 'green') +
            colored(" \u25BC ", 'yellow') +
            colored("#" * 34 + "]", 'blue'))
        print(
            colored("["+"#" * 88+"]", 'blue'))

        create_report(
            service=services_found,
            path=DIR_REPORTS
            )

        with open(DATA_JSON, 'w') as f:
            try:
                json.dump(services_found, f, indent=4)
            except Exception as e:
                print("Error: %s" % str(e))
    else:
        print(
            "Error: The service ID was not found in the Backbone."
            " It is not possible to create the report because is empty.")
        with open(DATA_JSON, 'w') as f:
            try:
                json.dump(services_found, f, indent=4)
            except Exception as e:
                print("Error: %s" % str(e))


if __name__ == "__main__":
    main()
